FROM registry.gitlab.com/gitlab-org/terraform-images/stable:latest

RUN set -euxo pipefail ;\
    apk add --no-cache --update ansible ;\
    rm -rf /var/cache/apk/* ;\
    rm -rf /root/.cache

CMD ["/bin/sh"]